class UsersController < ApplicationController

    def index
      @user = User.first
      @achievement_death = @user.achievement_death
      @achievement_coin = @user.achievement_coin
      @achievement_turtle = @user.achievement_turtle_monster
      @achievement_bowser = @user.achievement_bowser_monster
    end
end
