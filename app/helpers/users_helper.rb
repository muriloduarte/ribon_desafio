module UsersHelper
  def return_trophy(level)
    case level
    when "zero"
      "#000000"
    when "one"
      "#6e4100"
    when "two"
      "#cc7722"
    when "three"
      "#cd7f32"
    when "four"
      "#c0c0c0"
    else
      "#FFC952"
    end
  end
end