class User < ApplicationRecord
    has_many :collected_coins
    has_many :killed_monsters
    has_many :deaths
    has_many :achievements
    has_one :achievement_coin
    has_one :achievement_death
    has_one :achievement_turtle_monster
    has_one :achievement_bowser_monster

    after_create :create_achivements

    def create_achivements
        Achievement.create([
                            {user: self, type: :AchievementCoin},
                            {user: self, type: :AchievementDeath},
                            {user: self, type: :AchievementTurtleMonster},
                            {user: self, type: :AchievementBowserMonster},
                            ])
    end
end
