class Death < ApplicationRecord
  belongs_to :user
  validates :user_id, presence: true

  after_create :update_balance_achievement

  def update_balance_achievement
      user.achievement_death.increase_balance
  end

end
