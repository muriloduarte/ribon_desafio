class KilledMonster < ApplicationRecord
  belongs_to :user
  belongs_to :monster

  validates :user_id, presence: true
  validates :monster_id, presence: true

  after_create :update_balance_achievement

  def update_balance_achievement
    if monster.is_a?(MonsterTurtle)
      user.achievement_turtle_monster.increase_balance
    else
      user.achievement_bowser_monster.increase_balance
    end
  end
  
end
