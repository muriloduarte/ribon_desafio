class AchievementCoin < Achievement
    def increase_balance(collected_coin_value)
        new_balance = balance + collected_coin_value
        update_attribute(:balance, new_balance)
        check_trophy_gain
    end
    
    private
        
    def check_trophy_gain
        case balance
        when 0
            update_attribute(:level, :zero) unless level.zero?
        when 1..100
            update_attribute(:level, :one) unless level.one?
        when 100..1000
            update_attribute(:level, :two) unless level.two?
        when 1000..10_000
            update_attribute(:level, :three) unless level.three?
        when 10_000..100_000
            update_attribute(:level, :four) unless level.four?
        else
            update_attribute(:level, :five) unless level.five?
        end
    end
end
