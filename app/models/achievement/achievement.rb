class Achievement < ApplicationRecord
    belongs_to :user

    extend Enumerize
    enumerize :level, in: [:zero, :one, :two, :three, :four, :five], default: :zero

    validates :user_id, presence: true
    validates :balance, presence: true
    validates :type, presence: true
end
