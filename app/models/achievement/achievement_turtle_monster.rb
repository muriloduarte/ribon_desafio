class AchievementTurtleMonster < Achievement
    def increase_balance
        new_balance = balance + 1
        update_attribute(:balance, new_balance)
        check_trophy_gain
    end
    
    private

    def check_trophy_gain
        case balance # a_variable is the variable we want to compare
        when 1
            update_attribute(:level, :one)
        when 100
            update_attribute(:level, :two)
        when 1000
            update_attribute(:level, :three)
        when 10_000
            update_attribute(:level, :four)
        when 100_000
            update_attribute(:level, :five)
        end
    end
end
