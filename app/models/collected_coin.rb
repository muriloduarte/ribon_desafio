class CollectedCoin < ApplicationRecord
  belongs_to :user
  validates :user_id, presence: true
  validates :value, presence: true

  after_create :update_balance_achievement

  def update_balance_achievement
      user.achievement_coin.increase_balance(value)
  end

end
