class Monster < ApplicationRecord
    has_many :killed_monsters

    validates :name, presence: true
    validates :type, presence: true

end
