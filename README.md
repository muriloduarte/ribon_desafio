# Desafio Ribon

> Esse desafio propõe o desenvolvimento de uma solução para administrar a geração de troféus de um jogo.

## Instalação

> É necessário instalar o Ruby e o Rails em seu computador para executar esse projeto. 

* Versão Ruby - 2.6.5p114
* Versão Rails - 6.0.1

> Recomendo seguir o tutorial do [Go Rails](https://gorails.com/setup/ubuntu/19.10).

> Após a instalação de depêndencias com o passo anterior execute os seguintes comandos na pasta do projeto:

```sh
bundle install 
```

```sh
yarn install
```

```sh
rake db:migrate
```

> Se desejar incluir alguns dados no BD automaticamente:

```sh
rake db:seed
```

## Executar


> Para rodar o projeto execute no terminal, estando dentro da pasta do projeto:

```sh
rails s
```
> Em seu browser acesse, para ver a página de troféus:

`http://localhost:3000`


## Incluindo dados

> A inclusão de dados pode ser feita via console, ou pelo arquivo seeds.rb.
