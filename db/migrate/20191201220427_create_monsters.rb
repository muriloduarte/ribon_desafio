class CreateMonsters < ActiveRecord::Migration[6.0]
  def change
    create_table :monsters do |t|
      t.string :name, presence: true
      t.string :type, presence: true
    end
  end
end
