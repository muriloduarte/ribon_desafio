class CreateAchievements < ActiveRecord::Migration[6.0]
  def change
    create_table :achievements do |t|
      t.integer :balance, presence: true, default: 0
      t.string :level, presence: true
      t.references :user, null: false, foreign_key: true
      t.string :type, presence: true

      t.timestamps
    end
  end
end
