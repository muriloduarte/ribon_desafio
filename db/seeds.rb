puts 'Criando Usuários'
User.create([{},{}])

puts 'Criando Monstros'
Monster.create([
        {name: "Pacific Rim", type: :MonsterTurtle},
        {name: "Godzilla", type: :MonsterBowser}
        ])

puts 'Criando Deaths'
(1..50).each do |u|
    Death.create(user: User.first)
end
    

puts 'Criando Colected Coin'
CollectedCoin.create([
    {user: User.first, value: 10},
    {user: User.first, value: 100},
    {user: User.first, value: 1000},
    {user: User.first, value: 10000},
    {user: User.first, value: 100000},

    ])

puts 'Criando killed Monster'

#turtle
(1..100).each do |u|
    KilledMonster.create(user: User.first, monster: Monster.first)
end

#bowser
KilledMonster.create(user: User.first, monster: Monster.second)
